package main

import (
    "fmt"
    "regexp"
)

func main() {
    var someText = "A1 asdf B3 asdf F5"

    // perform regex match
    r, _ := regexp.Compile("([a-zA-Z]+\\d)")
    res := r.FindAllStringSubmatch(someText, -1)

    fmt.Println()
    for i := range res {
        fmt.Printf("\t%s", res[i][1])
    }
}
