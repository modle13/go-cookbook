package main

import (
    "fmt"
    "github.com/atotto/clipboard"
)

func main() {
    // get clipboard
    clipText, _ := clipboard.ReadAll()
    fmt.Println(clipText)
}
